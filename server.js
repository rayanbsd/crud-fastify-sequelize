const fastify = require('fastify')({logger:true})
const PORT = process.env.PORT || 5000
fastify.register(require('./routes/films'))
fastify.register(require('./routes/users'))
fastify.register(require('./routes/posts'))
fastify.register(require('./routes/products'))
fastify.register(require('./routes/securitynumbers'))
/* import connection from "./database/index.js" */

/* fastify.get('/',(req,reply)=>{
    reply.send('hello world!')
}) */
// DB Connection
const connection =require('./database/index.js');

const startServer = async () =>{
    try {
        await connection.sync({force:false})
        await fastify.listen(PORT)
    } catch (error) {
        fastify.log.error(error)
        process.exit(1)
    }
}
startServer()