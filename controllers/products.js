const Product = require("../models/Product");
const { Op } = require("sequelize");

const errHandler = (err) => {
  console.log("errors", err);
  return "error: " + err;
};

const addProduct = async (req, reply) => {
  try {
    const product = await Product.create(req.body);
    reply.code(201).send(product);
  } catch (err) {
    reply.code(500).send(errHandler(err));
  }
};
const getProduct = async (req, reply) => {
  const product = await Product.findAll().catch(errHandler);
  reply.code(200).send(product);
};
const getProductSearch = async (req, reply) => {
  try {
    const test = req.query;
    const products = await Product.findAll({
      where: { content: req.query.name },
    });
    reply.code(200).send(products);
  } catch (error) {
    reply.code(500).send(errHandler(err));
  }
};
const getProductSearchLike = async (req, reply) => {
  try {
    const product = await Product.findAll({
      where: { content: { [Op.like]: `%${req.query.name}%` } },
    });
    reply.code(200).send(product)
  } catch (error) {
    reply.code(500).send(errHandler(err));
  }
};
module.exports = {
  addProduct,
  getProduct,
  getProductSearch,
  getProductSearchLike,
};
