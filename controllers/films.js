const Film = require("../models/Film");

const addFilm = async (req, reply) => {
  try {
    const film = await Film.create(req.body);
    reply.code(201).send(film);
  } catch (error) {
    reply.code(500).send();
  }
};

const getFilms = async (req, reply) => {
  try {
    const film = await Film.findAll();
    console.log(film);
    reply.code(201).send(film);
  } catch (error) {
    reply.code(500).send();
  }
};

module.exports = { addFilm, getFilms };
