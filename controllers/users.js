const User = require("../models/User");
const { Op } = require("sequelize");
const puppeteer = require("puppeteer");

const errHandler = (err) => {
  console.log("errors", err);
  return "error: " + err;
};

const addUser = async (req, reply) => {
  try {
    const user = await User.create(req.body);
    reply.code(201).send(user);
  } catch (err) {
    errHandler(err);
    reply.code(500).send(errHandler(err));
  }
};
const getUser = async (req, reply) => {
  try {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.goto("https://blog.risingstack.com", {
      waitUntil: "networkidle0",
    });
    const pdf = await page.pdf({ format: "A4" });
    console.log(pdf)
    await browser.close();
    reply.code(200).send("hello")
    /* const users = await User.findAll()
       reply.code(200).send(users) */
  } catch (err) {
    errHandler(err);
    reply.code(500).send(errHandler(err));
  }
};

module.exports = { addUser, getUser };
