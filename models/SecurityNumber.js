const Sequelize = require("sequelize")
const sequelize = require("../database/index.js");
const User = require('./User')
 const SecurityNumber= sequelize.define('security_numbers',{
    id:{
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    number:{
        type: Sequelize.INTEGER(11),
        allowNull: false,
        unique:true
    },
    test:{
        type: Sequelize.INTEGER(11),
        allowNull: false,
        unique:true
    },
  /*   user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        }
      }, */
})
User.hasOne(SecurityNumber,{foreignKey: 'user_id'})

module.exports=SecurityNumber