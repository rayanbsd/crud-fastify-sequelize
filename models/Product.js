const Sequelize = require("sequelize");
const sequelize = require("../database/index.js");

module.exports = sequelize.define("products", {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  content: { type: Sequelize.STRING(300),allowNull:false }});
