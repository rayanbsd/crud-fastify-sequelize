const sequelize = require('../database/connection.js')
const Sequelize = require ('sequelize')
const User = require('./models/User.js')
const Comment = sequelize.define('comments',{
    id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        autoIncrement:true,
        primaryKey:true
    },
    content:{
        type:Sequelize.STRING,
    },
    
})
Comment.bleongsTo(User);
/* User.hasMany(Comment); */

module.exports = Comment
