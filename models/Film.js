const Sequelize = require('sequelize')
const sequelize = require("../database/index.js");

module.exports = sequelize.define("films",{
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
    title:{
        type: Sequelize.STRING(300)
    },
    description: {
        type:Sequelize.STRING(300)
    }

})