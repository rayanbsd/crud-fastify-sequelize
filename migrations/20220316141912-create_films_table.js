"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("films", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      title:{
        type: Sequelize.STRING(300)
    },
    description: {
        type:Sequelize.STRING(300)
    }
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("films");
  },
};
