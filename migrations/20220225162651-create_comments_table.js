'use strict';


module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('comments',{
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(10)
      },
      content:{
        type: Sequelize.STRING(300)
      },
/*       user_id:{
        allowNull: false,
        primaryKey:true,
        references:{
          model:'user',
          key:'id'
        }
      } */

    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('comments');
  }
};
