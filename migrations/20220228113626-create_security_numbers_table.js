"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("security_numbers", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      number: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        unique: true,
      },
      test: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        unique: true,
      },
      user_id: {
        type: Sequelize.INTEGER(11),
        references: {
          model: "users",
          key: "id",
        },
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("security_numbers");
  },
};
