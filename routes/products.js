const {
  addProduct,
  getProduct,
  getProductSearch,
  getProductSearchLike
} = require("../controllers/products");

const ProductSchema = {
  type: "object",
  properties: {
    id: { type: "string" },
    content: { type: "string" },
  },
};
const postProductOpts = {
  schema: {
    /*   body: {
      type: "object",
      properties: {
        id: { type: "integer" },
        content: { type: "string" },
      },
    }, */
    response: {
      201: ProductSchema,
    },
  },
  handler: addProduct,
};
const getProductOpts = {
  schema: {
    response: {
      200: { type: "array", products: ProductSchema },
    },
  },
  handler: getProduct,
};
const getProductSearchOpts = {
  schema: {
    querystring:{
      name:{type:'string'}
    },
   /*  response: {
      200: {
        type: "array",
        products: ProductSchema,
      },
    }, */
  },
  handler: getProductSearch,
};
const getProductSearchLikeOpts = {
  schema:{
    querystring:{
      content:{type:'string'}
    }
  },
  handler:getProductSearchLike
}

function productRoutes(fastify, options, done) {
  fastify.post("/products", postProductOpts);
  fastify.get("/products", getProductOpts);
  fastify.get("/products/search", getProductSearchOpts);
  fastify.get("/products/searchlike",getProductSearchLikeOpts);
  done();
}

module.exports = productRoutes;
