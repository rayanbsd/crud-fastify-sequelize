const { addSecurityNumber,addSecurityNumberToUser } = require("../controllers/securitynumbers.js");
const {addUser} = require('../controllers/users.js')
const SNSchema = {
  type: "object",
  properties: {
    id: { type: "string" },
    number: { type: "string" },
    /* user_id: { type: "string" }, */
  },
};

const addSecurityNumberOpts = {
    schema:{
        response:{
            201:SNSchema
        },
    },
    handler: addSecurityNumber
}
const addSecurityNumberToUserOpts ={
    schema:{
        response:{
            201:SNSchema
        }
    },
    handler : addSecurityNumberToUser
}
function securitynumbersRoutes(fastify, options, done) {

  fastify.post("/securitynumbers", addSecurityNumberOpts);
  fastify.post("/securitynumbers/:id",addSecurityNumberToUserOpts)
  done(); 
}
module.exports = securitynumbersRoutes;
