const {addFilm, getFilms} = require('../controllers/films.js')

const postFilmOpts ={
    schema:{body:{},response:{}},
    handler:addFilm
}

const getFilmOpts ={
    schema:{response:""},
    handler:getFilms
}

function filmsRoutes(fastify,options,done){
    fastify.get("/film",getFilmOpts)
    fastify.post("/films",postFilmOpts)
    done()
}
module.exports = filmsRoutes