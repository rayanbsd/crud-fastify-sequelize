const { getPostsHandler } = require('../controllers/handlers/posts.js')
const {getPostsSchema} = require ('../controllers/schemas/posts.js')
const getPostOpts = {
    schema:getPostsSchema,
    handler:getPostsHandler,
}

const postRoutes = (fastify, options, done)=>{
  fastify.get('/',getPostOpts)
  done()
}
module.exports = postRoutes
