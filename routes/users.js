const {addUser,getUser} = require('../controllers/users.js')

const UserSchema = {
    type: "object",
    properties: {
      username: { type: "string" },
      passwd: { type: "string" },
    },
  };
const postUsersOpts = {
    schema:{
        response:{
            201:UserSchema
        },
    },
    handler: addUser
}
const getUsersOpts ={
    schema:{
        response:{
            201:UserSchema
        }
    },
    handler:getUser
}


function usersRoute(fastify,options,done){
    fastify.post('/users',postUsersOpts)
    fastify.get('/users',getUsersOpts)
    done()
}
module.exports = usersRoute